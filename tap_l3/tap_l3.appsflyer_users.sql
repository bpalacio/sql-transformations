DROP TABLE IF EXISTS tap_l3.appsflyer_users;

CREATE TABLE tap_l3.appsflyer_users AS

  --Sólo consideramos usuarios de quienes tenemos data desde su origen (install)
  --Al 20211215 --> ~%75 
     WITH
     installs as 
     (
        SELECT  
           distinct appsflyer_id
        FROM tap_l2.fact_appsflyer_events
        WHERE event_name = 'install'
     )

    SELECT 
      current_date as _appsflyer_users_audit_date,
      fae.media_source,
      fae.event_name,
      fae.channel,
      CASE 
        WHEN current_date - date(fae.install_time) >= 31 THEN 'viejo' 
        ELSE 'nuevo' 
      END as user_type,
      count(distinct fae.appsflyer_id) as qty_users
    FROM  tap_l2.fact_appsflyer_events as fae
    INNER JOIN installs
       ON (installs.appsflyer_id = fae.appsflyer_id)
    GROUP BY 1,2,3,4,5;